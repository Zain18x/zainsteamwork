package com.zainaziz.teamprojectsocialmediaappdesign.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zainaziz.teamprojectsocialmediaappdesign.Adapters.RecyclerViewAdapters;
import com.zainaziz.teamprojectsocialmediaappdesign.R;


public class FragmentFollowing extends Fragment {

    RecyclerView recyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_followers, container, false);
        //initialize adapter and data
        recyclerView=view.findViewById(R.id.recyclerView);
        RecyclerViewAdapters recyclerViewAdapters=new RecyclerViewAdapters(getActivity(),2);
        recyclerView.setAdapter(recyclerViewAdapters);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }
}