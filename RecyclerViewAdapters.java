/*
 * by AwoApp
 */

package com.zainaziz.teamprojectsocialmediaappdesign.Adapters;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;


import com.zainaziz.teamprojectsocialmediaappdesign.Models.UserModel;
import com.zainaziz.teamprojectsocialmediaappdesign.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class RecyclerViewAdapters extends RecyclerView.Adapter<RecyclerViewAdapters.RecyclerviewHolder> {

    LayoutInflater layoutInflater;
    Activity activity;
    ArrayList<UserModel> userModelArrayList;
    TextView tvTitle,tvUserName,tvStatus;
    ImageView imgProfile;
    int activeFragment;

    public RecyclerViewAdapters(Activity context,int activeFragment) {
        layoutInflater = LayoutInflater.from(context);
        this.activity = context;
        this.activeFragment = activeFragment;

        initializeSampleData();
    }

    @NonNull
    @Override
    public RecyclerviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        View view = layoutInflater.inflate(R.layout.layout_recyclerview, viewGroup, false);
        RecyclerviewHolder recyclerviewHolder = new RecyclerviewHolder(view, i);

        return recyclerviewHolder;
    }

    /*For same return item*/
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerviewHolder recyclerviewHolder, final int i) {
        setData(i);


    }

    private void setData(int i) {
        tvUserName.setText(userModelArrayList.get(i).getUserName());
        tvTitle.setText(userModelArrayList.get(i).getName());
        tvStatus.setText(userModelArrayList.get(i).getStatus());

        imgProfile.setImageResource(userModelArrayList.get(i).getProfilePicture());

    }

    @Override
    public int getItemCount() {
        return userModelArrayList.size();
    }

    class RecyclerviewHolder extends RecyclerView.ViewHolder {
        public RecyclerviewHolder(@NonNull final View itemView, final int i) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            tvStatus = itemView.findViewById(R.id.tvStatus);

            if (activeFragment==1){tvStatus.setVisibility(View.GONE);}

        }

    }
    public void initializeSampleData(){
        userModelArrayList=new ArrayList<>();
        userModelArrayList.add(new UserModel("Tony Montana","montana_tony21","Friends",
                R.drawable.tony));
        userModelArrayList.add(new UserModel("Russell Crowe","russel_crowe","Follow",
                R.drawable.russel));
        userModelArrayList.add(new UserModel("Thomas Anderson","thomas_and","Follow Back",
                R.drawable.nep));
        userModelArrayList.add(new UserModel("Quentin Tarantino","tarantino_01","Friends",
                R.drawable.tarantino));
        userModelArrayList.add(new UserModel("Andrey Tarkovski","tarkovskiii","Follow",
                R.drawable.andrey));


    }

}
