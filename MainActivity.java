package com.zainaziz.teamprojectsocialmediaappdesign.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.zainaziz.teamprojectsocialmediaappdesign.Adapters.CustomViewPager;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentFollowers;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentFollowing;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentSuggest;
import com.zainaziz.teamprojectsocialmediaappdesign.Models.UserModel;
import com.zainaziz.teamprojectsocialmediaappdesign.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    CustomViewPager customViewPager;
    TabLayout mTableLayout;
    SearchView searchView;
    TextView tvMainTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize widgets
        viewPager=findViewById(R.id.viewPager);
        mTableLayout=findViewById(R.id.tablayout);
        searchView=findViewById(R.id.searchView);
        tvMainTitle=findViewById(R.id.tvMainTitle);

        //create pager adapter
        customViewPager=new CustomViewPager(getSupportFragmentManager());

        //add fragment to viewpager
        customViewPager.addFragment(new FragmentFollowers(),"Followers");
        customViewPager.addFragment(new FragmentFollowing(),"Following");
        customViewPager.addFragment(new FragmentSuggest(),"Suggest");
        //set pager adapter
        viewPager.setAdapter(customViewPager);
        mTableLayout.setupWithViewPager(viewPager);
        //set searchview hint
        searchView.setQueryHint("Search");
        searchView.setQuery("Search",false);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position){
                    case 0:
                        tvMainTitle.setText("Following");
                        break;
                    case 1:
                        tvMainTitle.setText("Followers");
                        break;
                    case 2:
                        tvMainTitle.setText("Suggested");
                        break;

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    //go to profile page
    public void goToProfilePage(View view) {
    startActivity(new Intent(MainActivity.this,ProfileActivity.class));
    }

    //sample adapter data

}