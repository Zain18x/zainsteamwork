package com.zainaziz.teamprojectsocialmediaappdesign.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.zainaziz.teamprojectsocialmediaappdesign.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}